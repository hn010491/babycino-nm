class Main {
    public static void main(String[] args) {
        //Demonstrates the logical ORing
        boolean a = true;
        boolean b = false;
        if (a || b) { 
            System.out.println(1); // Expected to print 1 since a is true.
        } else {
            System.out.println(0); // Prints a zero if it is incorrect
        }
    }
}