class Main {
    public static void main(String[] args) {
        if (trueMethod() || sideEffectMethod()) {
            System.out.println("Short-circuit works correctly.");
        } else {
            System.out.println("This should not be seen.");
        }
    }
    
    public static boolean trueMethod() {
        // This method returns true if enacted
        return true;
    }
    
    public static boolean sideEffectMethod() {
        // This method has a side effect, which should not be triggered
        System.out.println("Side effect method evaluated.");
        return false;
    }
}