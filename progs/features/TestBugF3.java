class Main {
    public static void main(String[] args) {
        if (falseMethod() || trueMethod()) {
            System.out.println("Expression evaluated to true.");
        } else {
            System.out.println("Expression evaluated to false. Bug F3 present.");
        }
    }
    
    public static boolean falseMethod() {
        // This method returns false
        return false;
    }
    
    public static boolean trueMethod() {
        // This method returns true
        return true;
    }
}